package hilos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Cliente {
    Socket cliente;
    
    public static void main(String args[]){
        java.awt.EventQueue.invokeLater(new Runnable(){
               public void run()
               {
                   new Cliente().run();
               }
        });                         
    }      
   void run()
    {
        try {
            cliente = new Socket("10.25.0.173",1001);
            
            
            PrintWriter out =
                new PrintWriter(cliente.getOutputStream(), true);
            
            BufferedReader in =
                new BufferedReader(
                    new InputStreamReader(cliente.getInputStream()));
            
            BufferedReader stdIn =
                    new BufferedReader(
                    new InputStreamReader(System.in));
            
            System.out.print("Conectado!");
            String sMensajeSalida = "";
            String sRespuesta = "";
            boolean bSalir = false;
            while(!bSalir)
            {
            System.out.print(">");
            sMensajeSalida = stdIn.readLine();
            out.println(sMensajeSalida);
            if(sMensajeSalida.toLowerCase().startsWith("salir"))
            {
                bSalir = true;
                break;
            }
            sRespuesta = in.readLine();
            System.out.printf("< : %s",sRespuesta);
            }
            cliente.close(); 
        } catch (IOException ex) {
            Logger.getLogger(Cliente.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}